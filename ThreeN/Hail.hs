module ThreeN.Hail (hail,storm,classifyParity) where
import ThreeN.Types
import ThreeN.Utils

classifyParity :: Int -> State
classifyParity x
  | x == 0 = O
  | even x = if even (evenToNth x) then K else L
  | odd x = if even (oddToNth x) then H else I
  | otherwise = undefined

hail :: Sq -> State -> Int -> [(State, Int, Sq)]
hail s N x = hail s (classifyParity x) x
hail s I x
  | (x == 1) && (s == 1) = [(I,1,1),(K,4,2),(L,2,3),(O,1,4)]
  | x == 1 = (O,x,s) : hail (s+1) O x
  | otherwise = (I,x,s) : hail (s+1) K (threeN1 x)
hail s H x = (H,x,s) : hail (s+1) L (threeN1 x)
hail s L x
  | even (oddToNth y) = (L,x,s) : hail (s+1) H y
  | otherwise = (L,x,s) : hail (s+1) I y
  where y = div x 2
hail s K x
  | even (evenToNth y) = (K,x,s) : hail (s+1) K y
  | otherwise = (K,x,s) : hail (s+1) L y
  where y = div x 2
hail _ O _ = []

storm :: Int -> [(State, Int, Sq)]
storm = hail 1 N
