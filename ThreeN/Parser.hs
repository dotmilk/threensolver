{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
module ThreeN.Parser (Parser(Parser)
                     ,item,sat,char,string
                     ,sepBy,sepBy1,chainl
                     ,chainl1,space,token
                     ,symb,apply,runParser,(<||>),(<|>),some,many) where

import Debug.Trace
import Control.Applicative (Alternative((<|>),empty) )
import Data.Functor
import Data.Foldable (asum)
import Control.Monad (guard)
import Control.Monad.Trans.State.Strict
import Data.Char (isSpace, isDigit, ord)

--newtype Parser a = Parser (String -> [(a,String)])
newtype Parser a = Parser { unParser :: StateT String [] a }

runParser = runStateT . unParser

instance Functor Parser where
  fmap f p = Parser $ f <$> unParser p

instance Applicative Parser where
  pure a = Parser $ pure a --(\cs -> [(a,cs)])
  (<*>) :: Parser (a -> b) -> Parser a -> Parser b
  f <*> a = Parser $ unParser f <*> unParser a

-- instance Alternative List where
--   empty = []
--   (<|>) = (++) -- length xs + length ys = length (xs ++ ys)


instance Alternative Parser where
  empty = Parser empty
  (<|>) :: Parser a -> Parser a -> Parser a
  a <|> b = Parser $ unParser a <|> unParser b

instance Monad Parser where
  a >>= f = Parser $ StateT $ \s -> do
    (a', s') <- runParser a s
    runParser (f a') s'

(<||>) :: Parser a -> Parser a -> Parser a
a <||> b = Parser $ StateT $ \s -> case comb' (unParser a) (unParser b) s of
       [] -> []
       (x:xs) -> [x]

comb' (StateT a) (StateT b) s = a s <|> b s
{-# INLINE comb' #-}
many v = some v <||> pure []
some v = (:) <$> v <*> many v
-- many v = some v <|> pure []
-- some v = (:) <$> v <*> many v
item :: Parser Char
item = Parser . StateT $ \case
                            []     -> empty
                            (c:cs) -> pure (c,cs)


sat :: (Char -> Bool) -> Parser Char
sat p =  do
  c <- item
  guard $ p c
  pure c

char :: Char -> Parser Char
char = sat . (==)

string :: String -> Parser String
string "" = return ""
string (c:cs) = (:) <$> char c <*> string cs

sepBy :: Parser a -> Parser b -> Parser [a]
sepBy p sep = (p `sepBy1` sep) <||> pure []


sepBy1 :: Parser a -> Parser b -> Parser [a]
sepBy1 p sep = (:) <$> p <*> many (sep *> p)

-- chainl
--   :: (Alternative f, Monad f) => f a -> f (a -> a -> a) -> a -> f a
chainl :: Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = (p `chainl1` op) <||> pure a

-- chainl1
--   :: (Alternative m, Monad m) => m t -> m (t -> t -> t) -> m t
chainl1 :: Parser t -> Parser (t -> t -> t) -> Parser t
chainl1 p op = p >>= rest
  where
    rest a = (do
                 f <- op
                 b <- p
                 rest (f a b)) <||> pure a


space :: Parser String
space = many (sat isSpace)


token p = p <* space


symb = token . string

apply :: Parser a -> String -> [(a, String)]
apply p = runParser (space *> p )

expr, term, factor, digit :: Parser Int
expr   = term   `chainl1` addop
term   = factor `chainl1` mulop
factor = digit <||> (symb "(" *> expr <* symb ")")
digit  = subtract (ord '0') . ord <$> token (sat isDigit)

addop, mulop :: Parser (Int -> Int -> Int)
addop = (symb "+" $> (+)) <||> (symb "-" $> (-))
mulop = (symb "*" $> (*)) <||> (symb "/" $> div)
