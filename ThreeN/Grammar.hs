module ThreeN.Grammar (walkGrammar,twoStates,threeStates
                      ,fourStates,fiveStates
                      ,twoStateActions
                      ,threeStateActions
                      ,fourStateActions
                      ,fiveStateActions
                      ,showSolutions
                      ,constrainer
                      ,verifyGrammar
                      -- ,twoStateFilterActions
                      -- ,threeStateFilterActions
                      -- ,fourStateFilterActions
                      -- ,fiveStateFilterActions
                      ,twoStateStarts
                      ,threeStateStarts
                      ,fourStateStarts
                      ,fiveStateStarts
                      ,grammarToAction
                      ,tripletToStates) where

import Data.List (permutations)
import Data.Maybe
import Control.Monad(ap)
import ThreeN.Action
import ThreeN.Hail
import ThreeN.Types
import ThreeN.Utils


grammarToAction :: [State] -> Maybe Double ->  Maybe Double
grammarToAction = foldr (.) id . reverse .map chooseFn

verifyGrammar :: [State] -> Bool
verifyGrammar = foldr verifyPairs True . chunkIt

verifyTripletGrammer :: [TripletState] -> Bool
verifyTripletGrammer = foldr verifyTripletPairs True . chunkIt

verifyTripletPairs :: (TripletState,TripletState) -> Bool -> Bool
verifyTripletPairs (a,b) answer = case (a,b) of
  (KLH,LIK) -> answer
  (KLH,LHL) -> answer
  (HLH,LHL) -> answer
  (HLH,LIK) -> answer
  (KLI,KKK) -> answer
  (KLI,KLI) -> answer
  (KLI,KLH) -> answer
  (KLI,KKL) -> answer
  (KKL,IKL)  -> answer
  (KKL,HLI)  -> answer
  (KKL,IKK)  -> answer
  (KKL,HLH)  -> answer
  (LHL,HLH)  -> answer
  (LHL,IKL)  -> answer
  (LHL,HLI)  -> answer
  (LHL,IKK)  -> answer

verifyPairs :: (State, State) -> Bool -> Bool
verifyPairs (a,b) answer = case (a,b) of
  (K,K) -> answer
  (K,L) -> answer
  (L,H) -> answer
  (L,I) -> answer
  (H,L) -> answer
  (I,K) -> answer
  _     -> False

iToK :: Maybe Double -> Maybe Double
iToK Nothing = Nothing
iToK (Just x)
  | isInt x && classifyParity (floor x) /= I = Nothing
  | otherwise = Just (t3p1 x)

hToL :: Maybe Double -> Maybe Double
hToL Nothing = Nothing
hToL (Just x)
  | isInt x && classifyParity (floor x) /= H = Nothing
  | otherwise = Just (t3p1 x)

lToHorI :: Maybe Double -> Maybe Double
lToHorI Nothing = Nothing
lToHorI (Just x)
  | isInt x && classifyParity (floor x) /= L = Nothing
  | otherwise = Just (d2 x)

kToKorL :: Maybe Double -> Maybe Double
kToKorL Nothing = Nothing
kToKorL (Just x)
  | isInt x && classifyParity (floor x) /= K = Nothing
  | otherwise = Just (d2 x)

chooseFn :: State -> Maybe Double -> Maybe Double
chooseFn state
  | state == I = iToK
  | state == H = hToL
  | state == L = lToHorI
  | state == K = kToKorL
  | otherwise = id

nextState :: [State] -> [State]
nextState state = case last state of
  K -> [K,L]
  L -> [H,I]
  H -> [L]
  I -> [K]

--handleBranch :: [[State]] -> [State] -> [[State]]
handleBranch :: [a] -> [a] -> [[a]]
handleBranch origin next = case length next of
  1 -> [origin <> next]
  2 -> let
    a = head next
    b = last next
    in [origin <> pure a , origin <> pure b]

-- walkGrammar [] = []
-- walkGrammar (x:xs) =
--   handleBranch x (nextState x) <> walkGrammar xs
--walkGrammar = foldr (\x -> (<>) (handleBranch x (nextState x))) []
walkGrammar = foldr ((<>) . ap handleBranch nextState) []
nStates 0 out = out
nStates 1 out = out
nStates n out = nStates (n-1) $ walkGrammar out

oneState :: [[State]]
oneState = [[K],[L],[H],[I]]


threePerm :: [Maybe Double -> Maybe Double]
threePerm = map grammarToAction $ permutations [I,K,L]

getStarts :: [[b]] -> [b]
getStarts = map (\(x:_)->x)

twoStates :: [[State]]
twoStates = nStates 2 oneState

twoStateStarts :: [State]
twoStateStarts = getStarts twoStates

threeStates :: [[State]]
threeStates = nStates 3 twoStates

threeStateStarts :: [State]
threeStateStarts = getStarts threeStates

fourStates :: [[State]]
fourStates = nStates 4 threeStates

fourStateStarts :: [State]
fourStateStarts = getStarts fourStates

fiveStates :: [[State]]
fiveStates = nStates 5 fourStates

fiveStateStarts :: [State]
fiveStateStarts = getStarts fiveStates

twoStateActions :: [Maybe Double -> Maybe Double]
twoStateActions = map grammarToAction twoStates

threeStateActions :: [Maybe Double -> Maybe Double]
threeStateActions = map grammarToAction threeStates

fourStateActions :: [Maybe Double -> Maybe Double]
fourStateActions = map grammarToAction fourStates

fiveStateActions :: [Maybe Double -> Maybe Double]
fiveStateActions = map grammarToAction fiveStates

twoStateFilterActions :: [(State, Maybe Double -> Maybe Double)]
twoStateFilterActions = zip twoStateStarts twoStateActions

threeStateFilterActions :: [(State, Maybe Double -> Maybe Double)]
threeStateFilterActions = zip threeStateStarts threeStateActions

fourStateFilterActions :: [(State, Maybe Double -> Maybe Double)]
fourStateFilterActions = zip fourStateStarts fourStateActions

fiveStateFilterActions :: [(State, Maybe Double -> Maybe Double)]
fiveStateFilterActions = zip fiveStateStarts fiveStateActions

extract (Just a) = a

showSolutions
  :: (Integral b, RealFrac a, Num t) =>
     Int -> ( Maybe t -> Maybe a) -> [(Int, b, Bool)]
showSolutions nSol aTest = take nSol $ mapMaybe (`constrainer` aTest) [Just x | x <- [1..]]

constrainer
  :: (Integral b, RealFrac a, Num t) =>
     Maybe Int ->  (Maybe t -> Maybe a) -> Maybe (Int, b, Bool)
constrainer Nothing _ = Nothing
constrainer (Just x) test = let
      result = test $ Just (fromIntegral x)
      isWhole = isMaybeInt result
      isEq = (floor <$> result) == pure x
      in if isWhole then Just (x,floor (extract result),isEq) else Nothing


-- foo = map (showSolutions 3) threeStateFilterActions
-- taba :: [a] -> [a] -> [[a]]
-- taba l1 l2 = let shire = taba (tail l1) l2
--   in case l1 of
--     (x:xs) -> [ head l1: head (tail shire)  , tail l2 ]
--     [] -> [ [] , l2 ]

tripletToStates :: TripletState -> [State]
tripletToStates LIK = [L,I,K]
tripletToStates IKK = [I,K,K]
tripletToStates KKK = [K,K,K]
tripletToStates IKL = [I,K,L]
tripletToStates HLI = [H,L,I]
tripletToStates LHL = [L,H,L]
tripletToStates KKL = [K,K,L]
tripletToStates KLI = [K,L,I]
tripletToStates HLH = [H,L,H]
tripletToStates KLH = [K,L,H]
