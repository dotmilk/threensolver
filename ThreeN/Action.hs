module ThreeN.Action (constructActions,actions) where
import ThreeN.Types
import ThreeN.Utils
import ThreeN.Hail

chooseFn :: State -> Double -> Double
chooseFn state
  | state == I = t3p1
  | state == H = t3p1
  | state == L = d2
  | state == K = d2
  | otherwise = id

bd :: (State, b, c) -> (a -> Double) -> a -> Double
bd (s,_,_) acc
  | s == O = acc
  | otherwise = chooseFn s . acc

constructActions
  :: Foldable t => t (State, b, c) -> Double -> Double
constructActions =  foldr bd id -- bd id

actions :: Int -> [Double -> Double]
actions n = map (constructActions.storm) $ take n [1..]
