module ThreeN.Types (Sq,State(N,I,H,K,L,O),TripletState(LIK,IKK,KKK,IKL,HLI,LHL,KKL,KLI,HLH,KLH)) where

type Sq = Int
data State = N | I | H | K | L | O deriving (Show, Eq)

data TripletState = LIK | IKK | KKK | IKL | HLI | LHL | KKL | KLI | HLH | KLH deriving (Show, Eq)
-- data KLH' = LIK | LHL deriving (Show, Eq)
-- data HLH' = LHL | LIK deriving (Show, Eq)
-- data KLI' = KKK | KLI | KLH | KKL deriving (Show, Eq)
-- data KKL' = ILK | HLI | IKK | HLH deriving (Show, Eq)
-- data LHL' = HLH | IKL | HLI | IKK deriving (Show, Eq)
-- data HLI' = KKK | KLH | KKL | KLI  deriving (Show, Eq)
-- data IKL' = HLI | IKK | HLH | IKL deriving (Show, Eq)
-- data KKK' = KKK | KLH | KKL | LHL | KLI | LIK deriving (Show, Eq)
-- data IKK' = KKK | KKL | KLH | LHL | LIK | KLI deriving (Show, Eq)
-- data LIK' = KKK | KLH | KLI | LHL | KKL | LIK deriving (Show, Eq)
