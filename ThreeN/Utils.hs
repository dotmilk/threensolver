module ThreeN.Utils (oddToNth,evenToNth,isInt
                    ,isMaybeInt,catMaybes
                    ,threeN1,currents
                    ,stones,t3p1,d2
                    ,chunkIt) where

isInt :: RealFrac a => a -> Bool
isInt x = x == fromInteger (round x)

isMaybeInt :: RealFrac a => Maybe a -> Bool
isMaybeInt Nothing = False
isMaybeInt (Just x) = x == fromInteger (round x)

oddToNth :: Integral a => a -> a
oddToNth x = div (x + 1) 2

evenToNth :: Integral a => a -> a
evenToNth x = div x 2

catMaybes :: [Maybe a] -> [a]
catMaybes ls = [x | Just x <- ls]

threeN1 :: Int -> Int
threeN1 = (1 +).(* 3)

currents :: [(a, b, c)] -> [a]
currents = map (\(x,_,_)->x)

stones :: [(a, b, c)] -> [b]
stones = map (\(_,x,_)->x)

t3p1 :: Double -> Double
t3p1 = (+1).(*3)

d2 :: Double -> Double
d2 =  (/2)

chunkIt :: [a] -> [(a, a)]
chunkIt = zip <*> tail
