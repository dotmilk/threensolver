{-# LANGUAGE QuasiQuotes #-}
module ThreeN.Equation (constructEquation,chooseStr,grammarToEquation) where
import Data.String.Interpolate
import ThreeN.Types

threeNHole :: Show a => a -> String
threeNHole expr = [i|(3*(#{expr})+1)|]

oneHalfHole :: Show a => a -> String
oneHalfHole expr = [i|(#{expr})/2|]

endEquation :: Show a => a -> String
endEquation expr = [i|#{expr}=1|]

--grammarToEquation :: Show a => [State] -> [a -> String]
grammarToEquation grammar = (foldl (.) id $ reverse $ map chooseStr grammar) "x"

chooseStr :: Show a => State -> a -> String
chooseStr state
  | state == I = threeNHole
  | state == H = threeNHole
  | state == L = oneHalfHole
  | state == K = oneHalfHole
  | state == O = endEquation

--breakDown :: String -> (State, Int, Sq) -> String
breakDown :: (Eq a, Num a) => (State, b, a) -> String -> String
breakDown (s,n,sq) acc
  | sq == 1 = acc ++ chooseStr s "x"
  | s == O = chooseStr s acc
  | otherwise = chooseStr s acc

constructEquation :: [(State, Int, Sq)] -> String
constructEquation = foldr breakDown ""
