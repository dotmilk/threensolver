{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE MultiParamTypeClasses, FlexibleContexts #-}
module Main where
import Data.Maybe
import Data.String.Interpolate
import Language.Haskell.Interpreter
import ThreeN.Utils
import ThreeN.Types
import ThreeN.Hail
import ThreeN.Equation
import ThreeN.Action
import ThreeN.Grammar
import ThreeN.Parser

main :: IO ()
main =  mapM_ (print . showSolutions 3) [grammarToAction [K, L, H]]
--main = mapM_ putStrLn $ map show $ iter 3 $ (constructActions.storm) 27  --take 30 [iter 3 $ (constructActions.storm) x | x <- [1..]] -- take 30 $ findSolutions 2 30-- iter 2 (constructActions $ storm 47)


theConstraint :: (RealFrac a1, Integral b, Integral a2, Num t) => a2 -> (t -> a1) -> Maybe (a2, b, Bool)
theConstraint x test = let
  result = test $ fromIntegral x
  isWhole = isInt result
  isEq = floor result == x
  in if isWhole then Just (x,floor result,isEq) else Nothing
--mapM_ putStrLn $ map (show.take ) $ findSolutions 1000 10
iter :: (RealFrac a1, Integral b, Integral a2, Num t) =>
        Int -> (t -> a1) -> [(a2, b, Bool)]
iter nSolutions theTest = take nSolutions $ mapMaybe (`theConstraint` theTest) [1..]

findSolutions nSolutions nEqs = let
  !acts = actions nEqs
  in map (iter nSolutions) acts

-- map (\(x,_,_) -> x) $ showSolutions 10 $ grammarToAction [I, K, K]
-- list work
-- showSolutions 20 $ grammarToAction [K,K,K,K,K,K]
--  map (\(x,_,_) -> x)
-- map (\x -> storm x)
-- (a' \\ e')
-- a = showSolutions 20 $ grammarToAction [K]
-- b = showSolutions 20 $ grammarToAction [K,K]
-- c = showSolutions 20 $ grammarToAction [K,K,K]
-- d = showSolutions 20 $ grammarToAction [K,K,K,K]
-- e = showSolutions 20 $ grammarToAction [K,K,K,K,K]
-- a' =  map (\(x,_,_) -> x) a
-- b' =  map (\(x,_,_) -> x) b
-- c' =  map (\(x,_,_) -> x) c
-- d' =  map (\(x,_,_) -> x) d
-- e' =  map (\(x,_,_) -> x) e

--  mapM_ (print . showSolutions 3) [grammarToAction [K, L, H, L, H, L, H, L, H, L, H, L, H, L, H, L, H, L, H, L, H, L, H, L, H]]
-- mapM_ putStrLn $ map (show.take 10) $ findSolutions 1000 10
-- for 5 (((((3*(x)+1))/2)/2)/2)/2
-- a_n = 16 n - 11 (for all terms given)
-- for 10 (((((3*((x)/2)+1))/2)/2)/2)/2
-- a_n = 2 (16 n - 11) (for all terms given)

-- 5,21,37,53,69,85

-- runTst = filter (\(_,_,x)-> x) $ map (\x-> (x,tst x,isInt (tst x))) [1..1000]
-- -- for 10 (((((3*((x)/2)+1))/2)/2)/2)/2
-- tst x = (((3*(((3*(x)+1))/2)+1))/2)/2
-- primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71] :: [Int]
-- -- iter 32 $ (constructActions.storm) 1

-- \c -> case c of
--           '"' -> unexpectedCharParser '"'
--           _  -> c
